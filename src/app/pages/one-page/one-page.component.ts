import { Component, OnInit } from '@angular/core';
import { data, menuOnePage } from 'src/app/config-data';

@Component({
  selector: 'app-one-page',
  templateUrl: './one-page.component.html',
  styleUrls: ['./one-page.component.scss']
})
export class OnePageComponent implements OnInit {
  public menuOnePage: any[] = menuOnePage;
  public data = data;

  constructor() { }

  ngOnInit(): void { }

  onPage(item) {
    var onepage = document.getElementById(item);
    onepage.focus();
  }

  redirectToLink(link) {
    if (link !== undefined) {
      const win = window.open(link, '_blank', 'noopener noreferrer');
    }
  }
}
