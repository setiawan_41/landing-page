export const menuOnePage = [
    {
        icon:'fa-solid fa-id-card',
        menu:'profile'
      },
    {
      icon:'fa-solid fa-user',
      menu:'dashbord'
    },
    {
      icon:'fa-solid fa-qrcode',
      menu:'info'
    }
  ]


  export const data ={
    education:[
        {
            education:'SDN Negeri 013 Jakarta Timur',
             start:"2001 - 2007",
             alamat:"Jl. Raya Condet No.Rt.007 /001, RT.8/RW.5, Gedong, Kec. Ps. Rebo, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13760",
             link:"https://www.google.com/maps/place/Jl.+Raya+Condet,+RT.8%2FRW.5,+Gedong,+Kec.+Ps.+Rebo,+Kota+Jakarta+Timur,+Daerah+Khusus+Ibukota+Jakarta/@-6.2998322,106.8539817,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f261576c3f25:0x153f0b89f8712658!8m2!3d-6.2998322!4d106.8561704?hl=id"
        },
        {
            education:'SMP Negeri 257 Jakarta Timur',
             start:"2007 - 2010",
             alamat:"Jalan Kel. Rambutan No.50, RT.4/RW.3, Rambutan, Kec. Ciracas, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13830",
        },
        {
            education:'SMA Teladan 1 Jakarta Timur',
             start:"2010 - 2013",
             alamat:"Jl. Madrasah No.49, RT.10/RW.5, Susukan, Kec. Ciracas, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13750",
             jurusan: 'IPA'
        },{
            education:'UNIVERSITAS INDRAPRASA PGRI JAKARTA',
             start:"2014 - 2018",
             alamat:"TB. Simatupang, Jl. Nangka Raya No.58 C, RW.5, Tj. Bar., Kec. Jagakarsa, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12530",
             jurusan: 'Sarjana Teknik Informatika'
        }
    ],
    socmed:[
        {
          socmed:'whatsapp',
          index:'+6281316192203',
          link:'https://wa.me/6281316192203'
        },
        {
          socmed:'facebook',
          index:'@setiawan ustamn'
        },
        {
            socmed:'instagram',
            index:'@setiawan80'
          }
    ],
    skill:[
      {
        skill:'html',
        des:'Dasar Bahasa dalam membuat web, Bahasa makrup yang di lakukan oleh tag contoh <div></div>, <p></p> dll'
      },
      {
        skill:'css',
        des:'Css salah satu styling language yang bisa guanakan dalam membuat web'
      },
      {
        skill:'scss',
        des:'Scss memberikan fitur yang tidak dimiliki CSS seperti variabel, nesting, mixins, inheritance yang bisa guanakan dalam membuat web'
      },
      {
        skill:'Angular',
        des:'Salah satu development framework membuat suatu web'
      },
      {
        skill:'Boostrap',
        des:'Suatu library dalam membuat design di dalam suatu web'
      },
    ]
  }